from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import *
from .forms import MataKuliahForm
from django.shortcuts import render


def webpage1(request):
    return render(request, 'home/pageSelamatDatang.html')

def webpage2(request):
    return render(request, 'home/tambah.html')

#def webpage3(request):
#    return render(request, 'home/hapus.html')

def webpage4(request):
    matkuls = MataKuliah.objects.all()
    context = {'matkuls':matkuls}
    return render(request, 'home/lihat.html', context)

def webpage5(request):
    matkuls = MataKuliah.objects.all()
    context = {'matkuls':matkuls}
    return render(request, 'home/lihatNonDetail.html', context)

def tambahSubmisi(request):
    form = MataKuliahForm()
    if (form.is_valid and request.method == 'POST'):
        form = MataKuliahForm(request.POST)
        form.save()
        return redirect('/lihatNonDetail')
    else:
        return redirect('/tambah')
    context = {'form':form}
    return render(request, 'home/lihatNonDetail.html', context)

def updateMatkul(request, pk):
    matkul = MataKuliah.objects.get(id=pk)
    form = MataKuliahForm(instance=matkul)

    if (form.is_valid and request.method == 'POST'):
        form = MataKuliahForm(request.POST, instance=matkul)
        form.save()
        return redirect('/lihatNonDetail')

    context = {'form':form}
    return render(request, 'home/matkul_update.html', context)

def deleteMatkul(request, pk):
    matkul = MataKuliah.objects.get(id=pk)
    
    if request.method == "POST":
        matkul.delete()
        return redirect('/lihatNonDetail')

    context = {'matkul':matkul}
    return render(request, 'home/matkul_delete.html', context)


def detailMatkul(request, pk):
    matkul = MataKuliah.objects.all().filter(id=pk)
    context = {'matkul':matkul}
    return render(request, 'home/lihatDetail.html', context)


