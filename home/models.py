from django.db import models
from django.utils import timezone
from datetime import datetime, date

class MataKuliah(models.Model):
    nama_matkul = models.CharField(max_length=30)
    nama_dosen = models.CharField(max_length=30)
    jumlah_sks = models.CharField(max_length=10)
    deskripsi_matkul = models.CharField(max_length=30)
    smt_tahun = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_matkul



