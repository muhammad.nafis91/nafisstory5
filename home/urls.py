"""tugas5ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.webpage1, name='webpage1'),
    path('tambah/', views.webpage2, name='webpage2'),

    path('lihat/', views.webpage4, name='webpage4'),
    path('lihatNonDetail/', views.webpage5, name='webpage5'),
    path('tambahSubmisi/', views.tambahSubmisi, name='tambahSubmisi'),
    path('update_matkul/<str:pk>/', views.updateMatkul, name='update_matkul'),
    path('delete_matkul/<str:pk>/', views.deleteMatkul, name='delete_matkul'),
    path('detail_matkul/<str:pk>/', views.detailMatkul, name='detail_matkul'),
]
